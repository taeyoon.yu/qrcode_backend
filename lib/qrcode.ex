defmodule Qrcode do
    @moduledoc """
    자산을 QRCODE로 관리하기 위한 Application 입니다.

    Updated at: 2021-07-04
    """
  use GenServer
  #client side

  @doc """
    Application을 시작합니다.
  """
  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :asset_management)
    # wait_add_asset()
  end


  @doc """
    (Test용)
    현재의 자산을 보여주고, 자산 입력을 기다립니다.

    자산을 입력 받은 후에는, 다시 자산을 보여주고 자산 입력 기다림을 반복합니다.
    ```
    iex> 2 = 2
    ```
  """
  def wait_add_asset() do
    IO.puts "#########################"
    IO.puts "Current Asset list is: "
    case get_total_asset() do
      [] -> "Enpty\n" |> IO.puts
      list -> list |> IO.puts
    end
    IO.puts "#########################"
    IO.puts "Please input asset number"
    IO.read(:stdio, :line) |> add_asset
    wait_add_asset()
  end

  @doc """
    자산을 추가하는 함수입니다.

    입력값으로 자산번호를 사용합니다.
  """
  def add_asset(asset_number) do
    GenServer.cast(:asset_management, {:add_asset, asset_number})
  end


  @doc """
    Excel 정보를 추가하는 함수 입니다.

    입력값으로 Json Array를 받습니다.
  """
  def add_excel_asset(asset_informations) do
    asset_informations = asset_informations
    |> Enum.map(fn x -> Map.merge(x, %{"asset_check_time" => "none", "asset_count" => "none", "asset_comment" => ""}) end)
    GenServer.cast(:asset_management, {:add_excel_asset, asset_informations})
  end

  @doc """
    Load 한 Json 데이터 정보를 추가하는 함수 입니다.

    입력값으로 Json Array를 받습니다.
  """
  def add_json_asset(asset_informations) do
    GenServer.cast(:asset_management, {:add_excel_asset, asset_informations})
  end


  @doc """
    전체 자산을 확인하는 함수입니다.

    전체 자산 리스트가 출력 됩니다.
  """
  def get_total_asset() do
    GenServer.call(:asset_management, :get_total_asset)
  end

  @doc """
    자산 번호를 인식할 수 있는 QRCODE를 생성합니다.

    입력값으로 자산번호를 사용합니다.

    자산번호 이름의 png 파일이 출력됩니다.
  """
  def generate_qrcode(asset_number) do
    result = asset_number
    |> EQRCode.encode()
    |> EQRCode.png()
    File.write("#{asset_number}.png", result, [:binary])
  end

  @doc """
    자산 리스트를 저장힙니다.

    입력값으로 파일 이름을 사용합니다.

    현재의 폴더에 파일이 저장됩니다.
  """
  def save(filename) do
    GenServer.cast(:asset_management, {:save, filename})
  end


  @doc """
    자산 리스트를 불러옵니다.

    입력값으로 파일 이름을 사용합니다.

    불러온 자산리스트는 자동으로 사용됩니다.
  """
  def load(filename) do
    GenServer.cast(:asset_management, {:load, filename})
  end

  @doc """
    인식된 자산을 업데이트 합니다.

    입력값으로 자산 1개의 map을 사용하고,

    map을 출력합니다.
  """
  def update_recognized_asset(target) do
    time = (:os.system_time(:second) + 32400) |> DateTime.from_unix! |> Kernel.to_string |> String.slice(0..18)
    count = case target["asset_count"] do
      "none" -> 1
      _ -> target["asset_count"] + 1
    end
    Map.merge(target, %{"result" => "success"})
    |> Map.put("asset_check_time", time)
    |> Map.put("asset_count", count)
  end



  @doc """
    전체 자산에서 인식된 자산을 업데이트 합니다.

    입력값으로 업데이틀 위한 시간과 횟수를 사용하고

    전체 자산 정보를 출력합니다.

  """
  def update_total_asset_by_recognized(new_asset) do
    result = get_total_asset() |> Enum.map(fn
      map ->
        if map |> Map.get("asset_number") ==  new_asset["asset_number"] do
          map
          |> Map.put("asset_count", new_asset["asset_count"])
          |> Map.put("asset_check_time", new_asset["asset_check_time"])
        else
          map
        end
    end)
    GenServer.cast(:asset_management, {:update_total_asset, result})
  end

  @doc """
    전체 자산에서 사용자가 입력한 내용으로 자산을 업데이트 합니다.

    입력값으로 업데이틀 위한 시간과 횟수를 사용하고

    전체 자산 정보를 출력합니다.

  """
  def update_total_asset_by_user(changed_asset) do
    result = get_total_asset() |> Enum.map(fn
      map ->
        if map |> Map.get("asset_number") ==  changed_asset["asset_number"] do
          map
          |> Map.put("asset_where", changed_asset["asset_where"])
          |> Map.put("asset_comment", changed_asset["asset_comment"])
        else
          map
        end
    end)
    GenServer.cast(:asset_management, {:update_total_asset, result})
  end

  ##############
  # server side
  ##############

  @doc """
    GenServer를 위한 초기 설정입니다.
  """
  def init(asset_list) do
    {:ok, asset_list}
  end

  @doc """
    자산 추가 시 사용되는 콜백 함수입니다.

    Cast로 출력값은 없습니다.
  """
  def handle_cast({:add_asset, asset_number}, asset_list) do
    {:noreply, [asset_number | asset_list]}
  end

  @doc """
    Excel에서 import 한 자산들 추가 시 사용되는 콜백 함수입니다.

    Cast로 출력값은 없습니다.
  """
  def handle_cast({:add_excel_asset, asset_informations}, asset_list) do
    {:noreply, asset_informations ++ asset_list}
  end

  @doc """
    전체 리스트를 업데이트하는 콜백 함수입니다.

    Cast로 출력값은 없습니다.
  """
  def handle_cast({:update_total_asset, asset_informations}, _asset_list) do
    {:noreply, asset_informations}
  end



  @doc """
    자산 저장 시 사용되는 콜백 함수입니다.

    출력값은 없습니다(Cast).
  """
  def handle_cast({:save, filename}, asset_list) do
    binary = :erlang.term_to_binary(asset_list)
    File.write(filename, binary)
    {:noreply, asset_list}
  end

  @doc """
    자산 리스트 저장 시 사용되는 콜백 함수입니다.

    출력값은 없습니다(Cast).
  """
  def handle_cast({:load, filename}, _asset_list) do
    {:ok, binary} = File.read(filename)
    {:noreply, :erlang.binary_to_term(binary)}
  end

  @doc """
    자산 리스트 출력 시 사용되는 콜백 함수 입니다.

    asset_list 가 출력됩니다.
  """
  def handle_call(:get_total_asset, _from, asset_list) do
    {:reply, asset_list, asset_list}
  end
end

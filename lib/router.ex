defmodule Qrcode.Router do
  use Plug.Router
  require Logger

  plug :match
  plug Plug.Parsers, parsers: [:json], pass: ["application/json"], json_decoder: Jason
  plug :dispatch
  # plug Plug.Static, at: "/home", from: :server

  get "/" do
    send_resp(conn, 200, "Hello!!")
  end

  get "/about/:user_name" do
    send_resp(conn, 200, "Hello! #{user_name}")
  end

  post "/api/assetData/add" do
    IO.inspect conn.body_params
    # Qrcode.add_asset(conn.body_params["asset_number"])
    response = case Qrcode.get_total_asset |> Enum.find(fn x-> x["asset_number"] == conn.body_params["asset_number"] end) do
      nil -> %{"result" => "error"}
      map ->
        result = Qrcode.update_recognized_asset(map)
        Qrcode.update_total_asset_by_recognized(result)
        result
    end
    send_resp(conn, 200, response |> Jason.encode!)
  end

  post "/api/assetData/update" do
    IO.inspect conn.body_params
    Qrcode.update_total_asset_by_user(conn.body_params)
    send_resp(conn, 200, "update success")
  end

  post "/api/excelAssetData/regist" do
    Qrcode.add_excel_asset(conn.body_params["_json"])
    response = Qrcode.get_total_asset
    send_resp(conn, 200, response |> Jason.encode!)
  end

  get "/api/totalAssetData/get" do
    response = Qrcode.get_total_asset
    send_resp(conn, 200, response |> Jason.encode!)
  end

  post "/api/jsonAssetData/regist" do
    Qrcode.add_json_asset(conn.body_params["_json"])
    response = Qrcode.get_total_asset
    send_resp(conn, 200, response |> Jason.encode!)
  end

  match _, do: send_resp(conn, 404, "404 error not found!")

end

defmodule Qrcode.Application do
    @moduledoc """
    자동 시작을 위한 Application behaviour 입니다.
    """

  use Application
  require Logger

  @doc """
    Application을 시작합니다.
  """
  def start(_type, _args) do
    qr_logic_start()
    web_server_start()
  end

  defp qr_logic_start do
    Qrcode.start_link() |> IO.inspect
    Logger.info "Qrcode logic started!!"
  end

  defp web_server_start() do
    port = Application.get_env(:plug_ex, :cowboy_port, 33378)
    children = [
      {Plug.Cowboy, scheme: :http, plug: Qrcode.Router, port: port}
    ]
    Logger.info "Web server started!!"
    Supervisor.start_link(children, strategy: :one_for_one)
  end

end
